package cryptography.util;

/*
        implements simple substitution cipher
        Author: James Lyons
        Created: 2012-04-28
*/

import java.util.ArrayList;

public class SimpleSub {
    StringBuilder key; // = new StringBuilder("AJPCZWRLFBDKOTYUQGENHXMIVS");
    StringBuilder invkey;
    char[] alphabet = "ABCDEFGHIJKLMNOPQRSTUVWXYZ".toCharArray();

    // Constructor
    public SimpleSub(StringBuilder key) {
        assert key.length() == 26;
        this.key = key;
        invkey = new StringBuilder();
    }

    int count = 0;

    int a2i(char ch) {
        // ch = ch.upper();
        // System.out.println(++count + ": " + (ch - 'A') );
        return ch - 'A';
    }

    char i2a(int i) {
        i = i % 26;
        char[] arr = "ABCDEFGHIJKLMNOPQRSTUVWXYZ".toCharArray();
        //System.out.println("i2a index: " + i);
        return arr[i];
    }
    public StringBuilder decipher(StringBuilder text) {
//        boolean keep_punct = false;
        StringBuilder ret = new StringBuilder();

        if ( invkey.length() == 0 ) {
            for ( char i : alphabet ) {
                invkey.append(i2a(key.indexOf( Character.toString(i) )));
            }
        }

//        if( !keep_punct )
        text = new StringBuilder( text.toString().replaceAll("\\s", "") );


        for ( char c : text.toString().toCharArray()) {
            if ( Character.isLetter(c) ) {
                ret.append(invkey.charAt(a2i(c)));
            } else {
                ret.append( c );
            }
        }

        return ret;
    }

    public StringBuilder encipher(StringBuilder text) {
        StringBuilder ret = new StringBuilder();

        for (char c : text.toString().toCharArray() )
            if ( Character.isLetter(c) ) ret.append( key.charAt(a2i(c)) );
            else ret.append(c);

        return ret;
    }


}
